# ZOTERO - .bib generation

# Configuration file

In the directory `config`, copy the `template.ini` file and name it `zotero.ini`:

```
[DEFAULT]
fileName = paper-refs.bib
destDir = /out
tmpDir = /tmp/
nThreads = 6
limitZot = 50

[account]
email = firstname.lastname@domain.com
id = 123456
type = group
key = 1a2b3c4d5e6f7g8h9i0jklmnoprstuvwxyz
```

In general, only the Zotero credentials in the `[account]` section need to be adapted.

# Run the script

```bash
$ cd zotero
$ python3 zotero/main.py
```

By default, the output file `paper-refs.bib` will be stored in the `./out` directory.

# Using docker

Build the docker image:
```bash
$ docker build -t zotero:latest .
```

Run the docker image:
```bash
$ docker run -v$PWD/out:/out -t zotero
```