FROM python:3.6.9-alpine

RUN mkdir config

COPY ./requirements.txt /requirements.txt
COPY ./config/*.ini /config/

WORKDIR /

RUN pip3 install -r requirements.txt

COPY . /

#ENTRYPOINT [ "python3" ]

CMD [ "python3", "zotero/main.py" ]

